importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

const firebaseConfig = {
  apiKey: "AIzaSyChmseSTN-hPh0syqFOSIrsvmUt5PVY0Pc",
  authDomain: "miamed-8d477.firebaseapp.com",
  projectId: "miamed-8d477",
  storageBucket: "miamed-8d477.appspot.com",
  messagingSenderId: "1066519068760",
  appId: "1:1066519068760:web:99e35592a86ae983394a2d",
  measurementId: "G-G4W3663C6K",
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  console.log("Received background message ", payload);
});
