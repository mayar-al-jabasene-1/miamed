import * as types from "../ActionType";

const initState = {
    reports: [],
    loadingReports: true,
    reportsPro: [],
    loadingReportsPro: true,
    generalReport: [],
    loadinggeneralReport: true,
    generalExReport: [],
    loadinggeneralExReport: true,
    reportTargetGroup: [],
    loagingreportTargetGroup: true
}

const ReportReducer = (state = initState, action) => {
    switch (action.type) {
        case types.GET_ALL_REPORTS:
            return { ...state, reports: action.payload, loadingReports: false }
        case types.EMPTY_ALL_REPORTS:
            return { ...state, reports: action.payload, loadingReports: true }
        case types.GET_ALL_REPORTS_PRODUCTS:
            return { ...state, reportsPro: action.payload, loadingReportsPro: false }
        case types.EMPTY_ALL_REPORTS_PRODUCTS:
            return { ...state, reportsPro: action.payload, loadingReportsPro: true }
        case types.DOWNLOAD_VISITES_EXCEL:
            return { ...state }
        case types.DOWNLOAD_VISITES_PRODUCTS:
            return { ...state }
        case types.GET_GENERAL_REPORTS:
            return { ...state, generalReport: action.payload, loadinggeneralReport: false }
        case types.EXPORT_GENERAL_REPORTS:
            return { ...state }
        case types.GET_GENERALEX_REPORTS:
            return { ...state, generalExReport: action.payload, loadinggeneralExReport: false }
        case types.EXPORT_GENERALEX_REPORTS:
            return { ...state }
        case types.GET_GENERAL_TARGET_GROUP:
            return { ...state, reportTargetGroup: action.payload, loagingreportTargetGroup: false }
        case types.EXPORT_GENERAL_TARGET_GROUP:
            return { ...state }
        default:
            return state;
    }
};

export default ReportReducer;
