import React, { useState } from "react";
import useClickOutside from "../../../useClickOutside";
import { putProductCat } from "../../../Redux/Actions/ActionConfigProduct";
import { useDispatch } from "react-redux";

function EditProductCat({ t, arr, setproduct_cat_id }) {
  let dispatch = useDispatch();
  const [name, setName] = useState(arr.name);
  const [font_family, setfont_family] = useState(arr.font_family);
  const [Load, setLoad] = useState(false);
  let domNode = useClickOutside(() => {
    setproduct_cat_id(null);
  });
  const submit = (event) => {
    event.preventDefault();
    dispatch(
      putProductCat(arr.id, name, font_family, setLoad, setproduct_cat_id)
    );
  };
  console.log("font_family==>", font_family);
  return (
    <div className="one_info_proCat" ref={domNode}>
      <div className="div_info_proCat">
        <input
          className="input_edit_proCat"
          defaultValue={name}
          onChange={(e) => setName(e.target.value)}
          style={{
            fontFamily: name ? "Calibri Bold" : "GE SS TWO",
          }}
        />
        <select
          className="input_edit_proCat"
          value={font_family}
          onChange={(e) => setfont_family(e.target.value)}
          style={{
            fontFamily: font_family ? "Calibri Bold" : "GE SS TWO",
          }}
        >
          <option disabled="disabled" selected="selected">
            {t("product_type")}
          </option>
          <option
            style={{
              fontFamily: "Calibri Bold",
            }}
            value="AMPOULES"
          >
            Nexa-Bold
          </option>
          <option
            style={{
              fontFamily: "Calibri Bold",
            }}
            value="EYEDROPS"
          >
            Mont Heavy DEMO
          </option>
          <option
            style={{
              fontFamily: "Calibri Bold",
            }}
            value="IVS"
          >
            Calibri Bold
          </option>

          <option
            style={{
              fontFamily: "Calibri Bold",
            }}
            value="DUBAIBOLD"
          >
            Dubai Bold
          </option>
          {/*    <option
            style={{
              fontFamily: "Calibri Bold",
            }}
            value="NASALDROPS"
          >
            NASAL DROPS
        </option>*/}
        </select>
      </div>
      <div className="action_proCat">
        {Load ? (
          <i className="las la-spinner icon_edit_table"></i>
        ) : (
          <i className="las la-save edit_proCat" onClick={submit}></i>
        )}
      </div>
    </div>
  );
}

export default EditProductCat;
