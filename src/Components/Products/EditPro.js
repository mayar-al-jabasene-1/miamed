import React, { useEffect } from "react";
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import { AbstructVar } from "../General/GeneralComponent/AbstructVar";
import { useSelectorCustom } from "../General/GeneralComponent/useSelectorCustom";
import { AbstructLang } from "../General/GeneralComponent/AbstructLang";
import { VariableProducts } from "./VariableProducts";
import { FunctionsLoading } from "../Main/Statistics/FunctionsLoading";
import SelectWithSearch from "../General/GeneralComponent/SelectWithSearch";
import SelectCreatableWithSearch from "../General/GeneralComponent/SelectCreatableWithSearch";
import { putProduct } from "../../Redux/Actions/ActionProduct";
import { useTranslation } from "react-i18next";
import { CheckFont } from "../General/GeneralComponent/CheckLang";

function EditPro({ arr, open, setOpen }) {
  let { dispatch, langmiamed } = AbstructVar();
  const { measurement, loadingmeasurement, productCat, loadingproductCat } =
    useSelectorCustom();
  const { state, setState, handleChangeProducts, setLoad } = VariableProducts();
  const {
    LoadProductCatCustom,
    PostUnit_of_measureCustom,
    DeleteUnit_of_measureCustom,
    LoadMeasurementsCustom,
  } = FunctionsLoading();
  const { translate } = AbstructLang();
  const { t, i18n } = useTranslation();
  useEffect(() => {
    setState((prevState) => ({
      ...prevState,
      name: arr.name,
      color: arr.color,
      product_type: arr.product_type,
      internal_reference: arr.internal_reference,
      description: arr.description,
      product_category_id: arr.category_id,
      product_category_name: arr.category_name,
      measurement_unit_id: arr.measurement_unit_id,
      measurement_unit_name: arr.measurement_unit_name,
    }));
  }, []);
  const FunSubmit = (e) => {
    e.preventDefault();
    console.log("measurement_unit_id" + state.measurement_unit_id);
    console.log("product_category_id" + state.product_category_id);
    dispatch(
      putProduct(
        arr.id,
        state.name,
        state.description,
        state.internal_reference,
        state.product_category_id,
        state.measurement_unit_id,
        state.color,
        state.product_type,
        state.image,
        setLoad
      )
    );
  };
  return (
    <RModal
      open={open}
      onClose={() => {
        setOpen(false);
      }}
      center={true}
      classNames={{
        modal: "popAdd",
      }}
      focusTrapped={false}
    >
      <div className="popAddForm" dir={langmiamed == "ar" ? "rtl" : "ltr"}>
        <h3
          style={{ fontFamily: CheckFont(translate("editProduct")) }}
          className={langmiamed == "ar" ? "textRight margin_top_20" : ""}
        >
          {translate("editProduct")}
        </h3>
        <form>
          <div className="input_row11">
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-pencil-alt"></i>
                <span
                  style={{ fontFamily: CheckFont(translate("name")) }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("name")}
                </span>
              </div>
              <input
                defaultValue={state.name}
                style={{ fontFamily: CheckFont(state.name) }}
                type="text"
                onChange={(e) => handleChangeProducts(e.target.value, "name")}
              />
            </div>
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-pencil-alt"></i>
                <span
                  style={{ fontFamily: CheckFont(translate("description")) }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("description")}
                </span>
              </div>
              <input
                defaultValue={state.description}
                style={{ fontFamily: CheckFont(state.description) }}
                type="text"
                onChange={(e) =>
                  handleChangeProducts(e.target.value, "description")
                }
              />
            </div>
          </div>
          <div className="input_row11">
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-pencil-alt"></i>
                <span
                  style={{ fontFamily: CheckFont(translate("internalRef")) }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("internalRef")}
                </span>
              </div>
              <input
                type="number"
                defaultValue={state.internal_reference}
                style={{
                  fontFamily:
                    i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
                }}
                onChange={(e) =>
                  handleChangeProducts(e.target.value, "internal_reference")
                }
              />
            </div>
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-hand-pointer"></i>
                <span
                  style={{ fontFamily: CheckFont(translate("product_cat")) }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("product_cat")}
                </span>
              </div>
              <div style={{ marginLeft: "10px" }}>
                <SelectWithSearch
                  load={loadingproductCat}
                  title={translate("product_cat")}
                  array={productCat}
                  setId={(value) =>
                    handleChangeProducts(value, "product_category_id")
                  }
                  funb={LoadProductCatCustom}
                  name={state.product_category_name}
                  previousID={state.product_category_id}
                  typeCss={true}
                />
              </div>
            </div>
          </div>
          <div className="input_row11">
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-hand-pointer"></i>
                <span
                  style={{ fontFamily: CheckFont(translate("unit")) }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("unit")}
                </span>
              </div>
              <div style={{ marginLeft: "10px" }}>
                <SelectCreatableWithSearch
                  load={loadingmeasurement}
                  options={measurement}
                  title={translate("unit")}
                  post={PostUnit_of_measureCustom}
                  deleteFunction={DeleteUnit_of_measureCustom}
                  funb={LoadMeasurementsCustom}
                  setIdd={(value) =>
                    handleChangeProducts(value, "measurement_unit_id")
                  }
                  name={state.measurement_unit_name}
                  previousID={state.measurement_unit_id}
                  typeCss={true}
                />
              </div>
            </div>
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-hand-pointer"></i>
                <span
                  style={{
                    fontFamily:
                      i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
                  }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("product_type")}
                </span>
              </div>
              <select
                value={state.product_type}
                onChange={(e) =>
                  handleChangeProducts(e.target.value, "product_type")
                }
                style={{
                  fontFamily: "Calibri Bold",
                }}
              >
                <option
                  value="marketing_tools"
                  style={{
                    fontFamily: CheckFont(translate("Marketing Tools")),
                  }}
                >
                  Marketing Tools
                </option>
                <option
                  value="medicine_samples"
                  style={{
                    fontFamily: CheckFont(translate("Free Samples")),
                  }}
                >
                  Free Samples
                </option>
              </select>
            </div>
          </div>
          <div className="input_row11">
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-palette"></i>
                <span
                  style={{
                    fontFamily:
                      i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
                  }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("color")}
                </span>
              </div>
              <input
                defaultValue={state.color}
                type="color"
                onChange={(e) => handleChangeProducts(e.target.value, "color")}
              />
            </div>
            <div className="one_input_row">
              <div className="Gen_icon">
                <i className="las la-image"></i>
                <span
                  style={{
                    fontFamily:
                      i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
                  }}
                  className={langmiamed == "ar" ? "textAlignRight" : ""}
                >
                  {translate("image")}
                </span>
              </div>
              <input
                type="file"
                style={{
                  fontFamily: "Calibri Bold",
                }}
                onChange={(e) =>
                  handleChangeProducts(e.target.files[0], "image")
                }
              />
            </div>
          </div>
        </form>
        <div className="div_btn">
          <button
            onClick={FunSubmit}
            style={{
              fontFamily: i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
            }}
          >
            {state.Load ? "Loading ... " : <>{translate("edit")}</>}
          </button>
        </div>
      </div>
    </RModal>
  );
}

export default EditPro;
