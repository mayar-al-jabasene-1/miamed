import React, { useState } from 'react';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css'
import SelectWithSearch from '../../General/GeneralComponent/SelectWithSearch';
import MultiSelectCheckbox from '../../General/GeneralComponent/MultiSelectCheckbox';
import { FunctionsLoading } from './FunctionsLoading';
import { FilterStatisticExternalnew, FilterStatisticTarget, FilterStatisticsWorkPlan } from '../../../Redux/Actions/ActionStatistics';
import { AbstructVar } from '../../General/GeneralComponent/AbstructVar';
import { FilterGeneralEXReportss, FilterGeneralReportss, FilterGeneralTargetGroup, FundownloadReportTargetGroup, FundownloadReportssPro, FundownloadReportssProEX } from '../../../Redux/Actions/ActionReport';
import { useSelectorCustom } from '../../General/GeneralComponent/useSelectorCustom';
import { VariableStatistics } from './VariableStatistics';

function FilterStatistics({ t, langmiamed, params, excel }) {
    let { dispatch } = AbstructVar()
    const { LoadTargetTypeCustom, LoadMedicalRepCustom, LoadReportTypeCustom } = FunctionsLoading()
    const { target_types, loadingtarget_type, medicalRep, loadingmedicalRep, reportType, loadingReportType } = useSelectorCustom()
    const { state, setState, handleChangeStatistics, setLoad, setIsFilter,setLoadExport } = VariableStatistics()
    const [selectedOptions, setSelectedOptions] = useState([]);
    const handleChange = (range) => {
        const [startDatee, endDatee] = range;
        setState((prevState) => ({
            ...prevState,
            startDate: startDatee,
            endDate: endDatee
        }));
    };
    const filter = event => {
        event.preventDefault()
        switch (params) {
            case 'work_plan':
                dispatch(FilterStatisticsWorkPlan(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoad));
                break;
            case 'external_visit':
                dispatch(FilterStatisticExternalnew(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoad));
                break;
            case 'target_type':
                dispatch(FilterStatisticTarget(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoad));
                break;
            case 'work_plan_general':
                setIsFilter(true)
                dispatch(FilterGeneralReportss(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoad))
                break;
            case 'external_visite_general':
                setIsFilter(true)
                dispatch(FilterGeneralEXReportss(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoad))
                break;
            case 'target_type_general':
                setIsFilter(true)
                dispatch(FilterGeneralTargetGroup(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoad))
                break;
            default:
                break;
        }
    }
    const exportToExcel = event => {
        event.preventDefault()
        switch (excel) {
            case 'work_plan_excel':
                dispatch(FundownloadReportssPro(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoadExport));
                break;
            case 'external_visit_excel':
                dispatch(FundownloadReportssProEX(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoadExport));
                break;
            case 'target_group_excel':
                dispatch(FundownloadReportTargetGroup(state.startDate, state.endDate, state.target_type_id, state.report_type_id, selectedOptions, setLoadExport));
                break;
            default:
                break;
        }
    };
    return (
        <div className="newFilterrr" dir={langmiamed == "ar" ? "rtl" : "ltr"} >
            <div className='oneFilternew' >
                <DatePicker
                    selected={state.startDate}
                    onChange={handleChange}
                    startDate={state.startDate}
                    endDate={state.endDate}
                    selectsRange
                    placeholderText={t('selectDate')}
                    className='date_inn'
                />
            </div>
            <div className='oneFilternew' >
                <SelectWithSearch
                    load={loadingtarget_type}
                    typeCss={true}
                    title={t('target_type')}
                    array={target_types}
                    setId={(value) => handleChangeStatistics(value, 'target_type_id')}
                    funb={LoadTargetTypeCustom}
                    tt={true}
                />
            </div>
            <div className='oneFilternew' >
                <SelectWithSearch
                    load={loadingReportType}
                    typeCss={true}
                    title={t('reportType')}
                    array={reportType}
                    setId={(value) => handleChangeStatistics(value, 'report_type_id')}
                    funb={LoadReportTypeCustom}
                    tt={true}
                />
            </div>
            <div className='oneFilternew oneFilternew2' >
                <MultiSelectCheckbox
                    selectedOptions={selectedOptions}
                    setSelectedOptions={setSelectedOptions}
                    testarr={medicalRep}
                    typeCss={true}
                    funb={LoadMedicalRepCustom}
                    load={loadingmedicalRep}
                />
            </div>
            <button className='targetfilter_button' onClick={filter} >
                {
                    state.Load ? "Loading ..." : `${t('filter')}`
                }
            </button>
            {
                state.isFilter && (
                    <button className='targetfilter_button' onClick={exportToExcel} >
                        {
                            state.LoadExport ? "Loading ..." : `${t('excel')}`
                        }
                    </button>
                )
            }

        </div>
    )
}

export default FilterStatistics