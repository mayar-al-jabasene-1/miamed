import React, { useEffect } from "react";
import Page_Header from "../../General/Page_Header";
import { loadNotification } from "../../../Redux/Actions/ActionAuth";
import SinglePagenation from "../../General/Pagination/SinglePagenation";
import InnerLoader from "../../General/InnerLoader";
import moment from "moment";
import { AbstructVar } from "../../General/GeneralComponent/AbstructVar";
import { useSelectorCustom } from "../../General/GeneralComponent/useSelectorCustom";
import { PagenationCustom } from "../../General/GeneralComponent/PagenationCustom";
import { AbstructLang } from "../../General/GeneralComponent/AbstructLang";
import { CheckFont } from "../../General/GeneralComponent/CheckLang";
import { useTranslation } from "react-i18next";

function PageNotification() {
  const [t, i18n] = useTranslation();
  let { navigate, langmiamed, dispatch } = AbstructVar();
  const { notify, loadingNotify } = useSelectorCustom();
  const { translate } = AbstructLang();
  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(loadNotification());
  }, []);
  const { displayArr, pageCount, changePage } = PagenationCustom(notify, 12);
  const ToDetails = (work_plan_id, task_id, service) => {
    if (
      work_plan_id !== null &&
      task_id !== null &&
      service === "WorkPlanVisit"
    ) {
      navigate(`/work-plans/${work_plan_id}/task/${task_id}`);
    }
    if (service === "MedicalRepVacation") {
      navigate("/vacation");
    }
    if (service === "TargetGroup") {
      navigate("/target-group");
    }
    if (service === "ExternalVisit") {
      navigate("/external-visites");
    }
  };

  const replacePmAm = (time, language) => {
    if (i18n.language === "ar") {
      // Replace pm with م and am with ص
      return time.replace(/([APMapm]{2})/, (match) =>
        match === "AM" ? "ص" : "م"
      );
    } else {
      // Keep the original time format for other languages
      return time;
    }
  };

  const replacePmAmHouer = (time, language) => {
    console.log("time==>", time);
    console.log("language==>", time);
    if (i18n.language === "ar") {
      // Extract hours, minutes, and AM/PM from the time
      const [hours, minutes, ampm] = time.split(/[:\s]/);

      // Convert hours to 24-hour format
      const hours24 =
        ampm.toUpperCase() === "AM"
          ? hours === "12"
            ? "00"
            : hours
          : parseInt(hours, 10).toString();

      // Replace pm with م and am with ص
      return `${hours24}:${minutes} ${ampm.replace(/([APMapm]{2})/, (match) =>
        match.toUpperCase() === "AM" ? "ص" : "م"
      )}`;
    } else {
      // Keep the original time format for other languages
      return time;
    }
  };

  return (
    <div className={langmiamed == "ar" ? "main-content_ar" : "main-content_en"}>
      <main>
        <Page_Header
          langmiamed={langmiamed}
          header={translate("notification")}
          path={`${translate("home")} / ${translate("notification")} `}
        />
      </main>
      <div className="page-content">
        <div className="main_notify" dir={langmiamed == "ar" ? "rtl" : "ltr"}>
          <br></br>
          {loadingNotify == false ? (
            <div className="all_notify">
              {displayArr.map((nn) => {
                return (
                  <div
                    key={nn.id}
                    className={`one_notify ${
                      langmiamed == "ar" ? "boder_rightt" : "boder_leftt"
                    }`}
                    onClick={() =>
                      ToDetails(
                        nn.work_plan_id,
                        nn.work_plan_task_id,
                        nn.service
                      )
                    }
                  >
                    <span
                      style={{ fontFamily: CheckFont(nn.title) }}
                      className={`tit_notify ${
                        langmiamed == "ar" ? "textRight" : ""
                      }`}
                    >
                      {nn.title}
                    </span>
                    <span
                      style={{ fontFamily: CheckFont(nn.body) }}
                      className={langmiamed == "ar" ? "textRight" : ""}
                    >
                      {nn.body}
                    </span>
                    <span>
                      {moment(nn.created_at).format("YYYY-MM-DD")} /{" "}
                      {replacePmAmHouer(
                        moment(nn.created_at).format("HH:mm A")
                      )}
                    </span>
                  </div>
                );
              })}
            </div>
          ) : (
            <div className="div_loadd">
              <InnerLoader />
            </div>
          )}

          <SinglePagenation pageCount={pageCount} changePage={changePage} />
        </div>
      </div>
    </div>
  );
}

export default PageNotification;
