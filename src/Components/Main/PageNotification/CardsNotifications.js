import React from 'react'

function CardsNotifications({ notify, langmiamed, t }) {
    return (
        <div className='main_notify' dir={langmiamed == 'ar' ? "rtl" : "ltr"} >
            <br></br>
            <div className='all_notify' >
                {
                    notify.map((nn) => {
                        return (
                            <div className={`one_notify ${langmiamed == 'ar' ? "boder_rightt" : "boder_leftt"}`} >
                                <span className={`tit_notify ${langmiamed == 'ar' ? "textRight" : ""}`}>{nn.title}</span>
                                <span className={langmiamed == 'ar' ? "textRight" : ""}>{nn.body}</span>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default CardsNotifications