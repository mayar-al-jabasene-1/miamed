import React from 'react'
import Main from './Main'
import AllLayout from '../General/AllLayout'

function MainDash() {
    return (
        <div >
            <AllLayout />
            <Main />
        </div>
    )
}

export default MainDash