import React from "react";
import { CheckFont } from "../GeneralComponent/CheckLang";
import { useTranslation } from "react-i18next";

function NewPagenation({ target_group, currentPage, changePage }) {
  const { t, i18n } = useTranslation();

  return (
    <div className="paginationBttnsNew">
      {console.log(currentPage)}
      <button
        className="previousBttn"
        style={{ fontFamily: CheckFont(t("Previous")) }}
        onClick={() => changePage(currentPage - 1)}
      >
        {t("Previous")}
      </button>
      <span className="currentPage">{currentPage}</span>
      {target_group.length == 0 ? (
        <></>
      ) : (
        <button
          className="nextBttn"
          style={{ fontFamily: CheckFont(t("Next")) }}
          onClick={() => changePage(currentPage + 1)}
        >
          {t("Next")}
        </button>
      )}
    </div>
  );
}

export default NewPagenation;
