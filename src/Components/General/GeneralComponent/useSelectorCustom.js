import { useSelector } from "react-redux"

export const useSelectorCustom = () => {
    const target_types = useSelector(state => state.target.target_types)
    const loadingtarget_type = useSelector(state => state.target.loadingtarget_type)
    const medicalRep = useSelector(state => state.medical_rep.medicalRep)
    const loadingmedicalRep = useSelector(state => state.medical_rep.loadingmedicalRep)
    const reportType = useSelector(state => state.report_type.reportType)
    const loadingReportType = useSelector(state => state.report_type.loadingReportType)
    const generalReport = useSelector(state => state.reportss.generalReport)
    const loadinggeneralReport = useSelector(state => state.reportss.loadinggeneralReport)
    const generalExReport = useSelector(state => state.reportss.generalExReport)
    const loadinggeneralExReport = useSelector(state => state.reportss.loadinggeneralExReport)
    const reportTargetGroup = useSelector(state => state.reportss.reportTargetGroup)
    const loagingreportTargetGroup = useSelector(state => state.reportss.loagingreportTargetGroup)
    const notify = useSelector(state => state.auth.notify)
    const loadingNotify = useSelector(state => state.auth.loadingNotify)
    const cities = useSelector(state => state.countries.cities)
    const loadingCities = useSelector(state => state.countries.loadingCities)
    const specializations = useSelector(state => state.target.specializations)
    const loadingspecializations = useSelector(state => state.target.loadingspecializations)
    const TGbymedicalRep = useSelector(state => state.medical_rep.TGbymedicalRep)
    const loadingTG = useSelector(state => state.medical_rep.loadingTG)
    const target_group = useSelector(state => state.target.target_group)
    const loadingTargetGroup = useSelector(state => state.target.loadingTargetGroup)
    const countriess = useSelector(state => state.countries.countriess)
    const loadingCountries = useSelector(state => state.countries.loadingCountries)
    const senior_supervisor = useSelector(state => state.auth.senior_supervisor)
    const loadingsenior_supervisor = useSelector(state => state.auth.loadingsenior_supervisor)
    const workflow = useSelector(state => state.work_floww.workflow)
    const loadingWorkflow = useSelector(state => state.work_floww.loadingWorkflow)
    const product = useSelector(state => state.productt.product)
    const loadingproduct = useSelector(state => state.productt.loadingproduct)
    const measurement = useSelector(state => state.configProduct.measurement)
    const loadingmeasurement = useSelector(state => state.configProduct.loadingmeasurement)
    const productCat = useSelector(state => state.configProduct.productCat)
    const loadingproductCat = useSelector(state => state.configProduct.loadingproductCat)
    const oneWorkflow = useSelector(state => state.work_floww.oneWorkflow)
    const loadingoneWorkflow = useSelector(state => state.work_floww.loadingoneWorkflow)
    const external_visites = useSelector(state => state.externalVisites.external_visites)
    const loadingexternal_visites = useSelector(state => state.externalVisites.loadingexternal_visites)
    const Vacation = useSelector(state => state.vacationn.Vacation)
    const loadingVacation = useSelector(state => state.vacationn.loadingVacation)
    const roles = useSelector(state => state.roless.roles)
    const loadingRoles = useSelector(state => state.roless.loadingRoles)
    const permission = useSelector(state => state.roless.permission)
    const loadingpermission = useSelector(state => state.roless.loadingpermission)
    const classifications = useSelector(state => state.target.classifications)
    const loadingclassifications = useSelector(state => state.target.loadingclassifications)
    const users = useSelector(state => state.auth.users)
    const loadingUsers = useSelector(state => state.auth.loadingUsers)
    const requests = useSelector(state => state.request.requests)
    const loadingRequests = useSelector(state => state.request.loadingRequests)
    const politics = useSelector(state => state.politicss.politics)
    const loadingpolitics = useSelector(state => state.politicss.loadingpolitics)
    return {
        target_types,
        loadingtarget_type,
        medicalRep,
        loadingmedicalRep,
        reportType,
        loadingReportType,
        generalReport,
        loadinggeneralReport,
        generalExReport,
        loadinggeneralExReport,
        reportTargetGroup,
        loagingreportTargetGroup,
        notify,
        loadingNotify,
        cities,
        loadingCities,
        specializations,
        loadingspecializations,
        TGbymedicalRep,
        loadingTG,
        target_group,
        loadingTargetGroup,
        countriess,
        loadingCountries,
        senior_supervisor,
        loadingsenior_supervisor,
        workflow,
        loadingWorkflow,
        product,
        loadingproduct,
        measurement,
        loadingmeasurement,
        productCat,
        loadingproductCat,
        oneWorkflow,
        loadingoneWorkflow,
        external_visites,
        loadingexternal_visites,
        Vacation,
        loadingVacation,
        roles,
        loadingRoles,
        permission,loadingpermission,
        classifications,loadingclassifications,
        users,loadingUsers,
        requests,loadingRequests,
        politics,loadingpolitics
    }
}