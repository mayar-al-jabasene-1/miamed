import { useState } from "react";

export const HandleFilter = () => {
    const [records, setRecords] = useState([]);
    function handleFilter(event,arr) {
        const newData = arr.filter(row => {
            return row.name.toLocaleLowerCase().includes(event.target.value.toLocaleLowerCase())
        })
        setRecords(newData)
    }
    return{
        records,
        handleFilter
    }
}