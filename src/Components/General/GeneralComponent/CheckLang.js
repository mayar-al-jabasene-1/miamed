function CheckLang(text) {
  const arabicRegex = /[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF]/;
  return arabicRegex.test(text);
}

function CheckFont(text) {
  let check = CheckLang(text);
  if (check) {
    return "GE SS TWO";
  } else {
    return "Calibri Bold";
  }
}

export { CheckFont };

function CheckArabic(text, fontFamily) {
  console.log(fontFamily);
  let check = CheckLang(text);
  if (check) {
    return "GE SS TWO";
  } else {
    return fontFamily;
  }
}

const replacePmAm = (time, i18n) => {
  if (i18n.language === "ar") {
    // Replace pm with م and am with ص
    return time.replace(/([APMapm]{2})/, (match) =>
      match === "AM" ? "ص" : "م"
    );
  } else {
    // Keep the original time format for other languages
    return time;
  }
};

const replacePmAmHouer = (time, i18n) => {
  console.log("time==>", time);
  console.log("language==>", time);
  if (i18n.language === "ar") {
    // Extract hours, minutes, and AM/PM from the time
    const [hours, minutes, ampm] = time.split(/[:\s]/);

    // Convert hours to 24-hour format
    const hours24 =
      ampm.toUpperCase() === "AM"
        ? hours === "12"
          ? "00"
          : hours
        : parseInt(hours, 10).toString();

    // Replace pm with م and am with ص
    return `${hours24}:${minutes} ${ampm.replace(/([APMapm]{2})/, (match) =>
      match.toUpperCase() === "AM" ? "ص" : "م"
    )}`;
  } else {
    // Keep the original time format for other languages
    return time;
  }
};

export { CheckArabic, replacePmAm, replacePmAmHouer };
