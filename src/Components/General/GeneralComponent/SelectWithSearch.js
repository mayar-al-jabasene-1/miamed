import React, { useState, useEffect } from "react";
import ReactLoading from "react-loading";
import useClickOutside from "../../../useClickOutside";
import { useTranslation } from "react-i18next";
import { CheckFont } from "./CheckLang";
let selectedId = null;
function SelectWithSearch({
  array,
  load,
  typeCss,
  title,
  left,
  setId,
  funb,
  isEmpty,
  name,
  previousID,
  loadID,
  func,
}) {
  let langmiamed = localStorage.getItem("langmiamed");
  const { t, i18n } = useTranslation();
  const [open, setopen] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [filtered, setFiltered] = useState([]);
  const [newArr, setnewArr] = useState([]);
  useEffect(() => {
    console.log(isEmpty);
    if (isEmpty === true) {
      setSearchValue("");
    }
    setSearchValue(name);
  }, [isEmpty, name]);
  useEffect(() => {
    const updatedArray = array.map((item) => {
      return { id: item.id, name: t(item.name) };
    });
    setnewArr(updatedArray);
  }, [array]);
  useEffect(() => {
    setFiltered(newArr);
  }, [newArr]);
  let domNode = useClickOutside(() => {
    setopen(false);
  });
  const handleInputChange = (e) => {
    const inputValue = e.target.value;
    setSearchValue(inputValue);
    const filtered = newArr.filter((item) =>
      item.name.toLowerCase().includes(inputValue.toLowerCase())
    );
    setFiltered(filtered);
  };

  const handleOptionClick = (item) => {
    setSearchValue(t(item.name));
    if (previousID) {
      setId(previousID);
    }
    console.log(item.id);
    setId(item.id);
    selectedId = item.id;
    setopen(false);
  };
  const openDropDown = (e) => {
    e.preventDefault();
    if (funb) {
      funb();
    }
    if (func) {
      func(loadID);
    }
    setopen(true);
  };

  console.log("ttttt=<>", i18n.language);
  return (
    <div className={left ? "div_creatable nomarginLeft" : "div_creatable"}>
      <div
        className={typeCss ? "view_creatable1 " : "view_creatable "}
        onClick={openDropDown}
      >
        <span
          style={{
            fontFamily: searchValue
              ? CheckFont(searchValue)
              : i18n.language === "ar"
              ? "GE SS TWO"
              : "Calibri Bold",
          }}
        >
          {searchValue || `${title ? title : t("select")}`}
        </span>
        <i className="las la-angle-double-down icon_searchselect"></i>
      </div>
      {open && (
        <div
          className="select_creatable"
          ref={domNode}
          dir={langmiamed === "ar" ? "rtl" : "ltr"}
        >
          <input
            type="text"
            value={searchValue}
            style={{
              fontFamily: searchValue
                ? CheckFont(searchValue)
                : i18n.language === "ar"
                ? "GE SS TWO"
                : "Calibri Bold",
            }}
            onChange={handleInputChange}
            placeholder={`${t("search")} ...`}
            className="input_custm_select"
          />
          {load == false ? (
            filtered.map((cc) => (
              <div
                className={`choose_creatable ${
                  searchValue === cc.name ? "active_creatable" : ""
                }`}
                key={cc.id}
                onClick={() => handleOptionClick(cc)}
              >
                <span style={{ fontFamily: CheckFont(t(cc.name)) }}>
                  {t(cc.name)}
                </span>
              </div>
            ))
          ) : (
            <div className="loadinselect">
              <ReactLoading
                type="spinningBubbles"
                color="#9699c2"
                height={"7%"}
                width={"7%"}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
}

export default SelectWithSearch;
