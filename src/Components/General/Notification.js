import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as XLSX from "xlsx";
import { CheckFont } from "./GeneralComponent/CheckLang";
import { AbstructLang } from "./GeneralComponent/AbstructLang";

const CustomToast = ({ mess }) => {
  const { translate } = AbstructLang();
  if (mess === "قيمة الحقل (رقم الهاتف) مُستخدمة من قبل") {
    return (
      <div style={{ fontFamily: CheckFont(mess) }}>
        {"قيمة الحقل (معرف المستخدم) مُستخدمة من قبل"}
      </div>
    );
  } else {
    return <div style={{ fontFamily: CheckFont(mess) }}>{mess}</div>;
  }
};
toast.configure();
export const notifyError = (mess) => {
  toast.error(<CustomToast mess={mess} />, {
    position: toast.POSITION.TOP_RIGHT,
    autoClose: 3000,
  });
};
export const notifyErrorNetWork = (mess) => {
  toast.error(<CustomToast mess={mess} />, {
    position: toast.POSITION.TOP_RIGHT,
    autoClose: false,
  });
};
export const notifysuccess = (mess) => {
  toast.success(<CustomToast mess={mess} />, {
    position: toast.POSITION.TOP_RIGHT,
    autoClose: 3000,
  });
};

export const base_url = "https://miamed.peaklink.site/";
/*export const base_url = "https://test.miamed.peaklink.site/";*/

export const exportFile = (array, name) => {
  const worksheet = XLSX.utils.json_to_sheet(array);
  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, worksheet, name);
  XLSX.writeFile(workbook, `${name}.xlsx`);
};
