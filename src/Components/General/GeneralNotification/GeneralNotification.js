import React from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function GeneralNotification({ title , body }) {
  const CustomToast = ({ title , body }) => {
    return (
      <div>
        <h3>{title}</h3>
        <span>{body}</span>
      </div>
    );
  };

  return (
    <>
      {toast.success(<CustomToast title={title} body={body} />, {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: 3000,
      })}
    </>
  );
}

export default GeneralNotification;