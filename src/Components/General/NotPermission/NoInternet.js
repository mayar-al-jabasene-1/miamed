import React, { useEffect, useState } from "react";
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import noint from "../../../images/icons/noInternet.png";
import { useTranslation } from "react-i18next";

function NoInternet({ open, setOpen }) {
  const { t, i18n } = useTranslation();
  useEffect(() => {
    console.log("internet");
  });
  return (
    <RModal
      open={open}
      onClose={() => {
        setOpen(false);
      }}
      center
      classNames={{
        modal: "popupWarning",
      }}
      focusTrapped={false}
    >
      <div className="warning">
        <img src={noint} />
        <span
          style={{
            fontFamily: "Calibri Bold",
          }}
        >
          No Internet
        </span>
        <span
          style={{
            fontFamily: "Calibri Bold",
          }}
        >
          Please Reload
        </span>
      </div>
    </RModal>
  );
}

export default NoInternet;
