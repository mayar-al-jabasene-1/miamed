import React from "react";
import notfound from "../../images/notFound.png";
import { useTranslation } from "react-i18next";

function OutNotFound() {
  const [t, i18n] = useTranslation();
  return (
    <div className="noper">
      <img src={notfound} />
      <br></br>
      <h3
        style={{
          fontFamily: "Calibri Bold",
        }}
      >
        Not Found
      </h3>
    </div>
  );
}

export default OutNotFound;
