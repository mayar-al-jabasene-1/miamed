export function SelectAllOption({ toggleSelectAll }) {
    return (
        <div>
            <input
                type="checkbox"
                value="select_all"
                onChange={toggleSelectAll}
            />
            <span>Select all</span>
        </div>
    );
}