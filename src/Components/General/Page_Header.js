import React from "react";
import { CheckFont } from "./GeneralComponent/CheckLang";

function Page_Header({ langmiamed, header, path, name, underheader }) {
  const CheckFonttwo = (text) => {
    console.log("text,", text);
    // Function to check if a segment of text contains Arabic characters
    const isArabic = (segment) => /[\u0600-\u06FF]/.test(segment);

    // Split the text into segments based on "/"
    const segments = text.split("/");

    // Determine font family for each segment
    const fontFamilies = segments.map((segment) =>
      isArabic(segment) ? "GE SS TWO" : "Calibri Bold"
    );

    // Join the font families back together with "/"
    return fontFamilies.join("/");
  };
  return (
    <div className="page-header">
      <h4 className={langmiamed == "ar" ? "textRight" : "textLeft"}>
        <span style={{ fontFamily: CheckFont(header) }}> {header} </span>
        {underheader && (
          <span style={{ fontFamily: CheckFont(underheader) }}>
            /{underheader}
          </span>
        )}
        {name && <span style={{ fontFamily: CheckFont(name) }}> / {name}</span>}
      </h4>
      <small className={langmiamed == "ar" ? "page_header_small" : ""}>
        {path}
      </small>
    </div>
  );
}

export default Page_Header;
