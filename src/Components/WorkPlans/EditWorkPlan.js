import React, { useState,useEffect } from 'react'
import Switch from "react-switch";
import { useDispatch, useSelector } from "react-redux";
import './WorkPlans.css'
import { loadMedicalRep } from '../../Redux/Actions/ActionMedicalRep';
import SearchSelect from '../General/SelectSearch';

function EditWorkPlan({ arr, setwork_plan_id }) {
    let dispatch = useDispatch();
    const medicalRep = useSelector(state => state.medical_rep.medicalRep)
    const loadingmedicalRep = useSelector(state => state.medical_rep.loadingmedicalRep)
    const [note, setnote] = useState(arr.note)
    const [start_date, setstart_date] = useState(arr.start_date)
    const [end_date, setend_date] = useState(arr.end_date)
    const [days_duration, setdays_duration] = useState(arr.days_duration)
    const [is_in_service, setis_in_service] = useState(arr.is_in_service === true ? 1 : 0)
    useEffect(() => {
        dispatch(loadMedicalRep())
    }, [])
    useEffect(() => {
        setFiltered(medicalRep)
    }, [medicalRep])
    const [open, setopen] = useState(false)
    const [searchValue, setSearchValue] = useState("");
    const [medical_rep_id, setmedical_rep_id] = useState("");
    const [filtered, setFiltered] = useState([]);
    const handleInputChange = (e) => {
        const inputValue = e.target.value;
        setSearchValue(inputValue);
        const filtered = medicalRep.filter((item) =>
            item.first_name.toLowerCase().includes(inputValue.toLowerCase())
        );
        setFiltered(filtered);
    };

    const handleOptionClick = (item) => {
        setSearchValue(item.first_name);
        setmedical_rep_id(item.id);
        setopen(false);
    };
    const activee = () => {
        const newValue = is_in_service === '0' ? '1' : '0';
        setis_in_service(newValue);
    }
    const post = () => {
        console.log(arr)
        setwork_plan_id(null)
    }
    return (
        <div className='oneworkplan' >
            <Switch className='switch_plan' onColor='#4285C5' checkedIcon={false} uncheckedIcon={false} height={20} width={40} handleDiameter={15} checked={is_in_service} onChange={activee} />
            <div className='head_plan' >
                <input className='date_work' type='date' defaultValue={start_date} />
                <i className="las la-long-arrow-alt-right"></i>
                <input className='date_work' type='date' defaultValue={end_date} />
            </div>
            <textarea className='textarea_work' defaultValue={note} />
            <div className='plan_day' >For <input className='num_work' type='number' defaultValue={days_duration} /><span className='plan_day_num' >Days</span></div>
            <div className='row_plann' >
                <div className='delegate_name' >
                    <i class="las la-user"></i>
                    <SearchSelect
                        openCity={open}
                        setopenCity={setopen}
                        searchValue1={searchValue}
                        handleInputChange={handleInputChange}
                        filteredCategories={filtered}
                        handleOptionClick={handleOptionClick}
                        load={loadingmedicalRep}
                        typeCss={true}
                    />
                    {/*<input className='rep_work' defaultValue={arr.medical_rep} />*/}
                </div>
                <div className='action_plan' >
                    <i class="las la-save icon_edit_table" ></i>
                </div>
            </div>
        </div>
    )
}

export default EditWorkPlan