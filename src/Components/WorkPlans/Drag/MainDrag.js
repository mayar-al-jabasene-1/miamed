import React, { useState } from 'react';
import { useDrop } from 'react-dnd';
import { PetCard } from './Dragfile';
import AllLayout from '../../General/AllLayout';
import Page_Header from '../../General/Page_Header';
import { useTranslation } from "react-i18next";

const PETS = [
    { id: 1, name: 'dog' },
    { id: 2, name: 'cat' },
    { id: 3, name: 'fish' },
    { id: 4, name: 'hamster' },
];

export const Basket = () => {
    let langmiamed = localStorage.getItem('langmiamed');
    const [t, i18n] = useTranslation();
    const [basket, setBasket] = useState([]);
    const [{ isOver }, dropRef] = useDrop({
        accept: 'pet',
        drop: (item) => {
            setBasket((basket) =>
                !basket.find((pet) => pet.id === item.id) ? [...basket, item] : basket
            );
        },
        collect: (monitor) => ({
            isOver: monitor.isOver()
        })
    });

    const handleDelete = (id) => {
        setBasket((basket) => basket.filter((pet) => pet.id !== id));
    };

    return (
        <>
            <AllLayout />
            <div className={langmiamed == "ar" ? "main-content_ar" : "main-content_en"}>
                <main>
                    <Page_Header header="Test" path="Home / test" />
                </main>
                <div className="page-content">
                    <div className='pets'>
                        {PETS.map((pet) => (
                            <PetCard
                                key={pet.id}
                                id={pet.id}
                                name={pet.name}
                                onDelete={handleDelete}
                            />
                        ))}
                    </div>
                    <div className='basket' ref={dropRef}>
                        {basket.map((pet) => (
                            <PetCard
                                key={pet.id}
                                id={pet.id}
                                name={pet.name}
                                onDelete={handleDelete}
                            />
                        ))}
                        {isOver && <div>Drop Here!</div>}
                    </div>
                </div>
            </div>
        </>
    );
};