import React from 'react';
import { useDrag } from 'react-dnd';

export const PetCard = ({ id, name, onDelete }) => {
  const [{ isDragging }, dragRef] = useDrag({
    type: 'pet',
    item: { id, name },
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    })
  });

  const handleDelete = () => {
    onDelete(id);
  };

  return (
    <div className='pet-card' ref={dragRef}>
      {name}
      {isDragging && <button onClick={handleDelete}>حذف</button>}
    </div>
  );
};