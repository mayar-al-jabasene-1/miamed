import React from "react";
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import { useDispatch } from "react-redux";
import { postTask } from "../../../Redux/Actions/ActionTask";
import { useSelectorCustom } from "../../General/GeneralComponent/useSelectorCustom";
import { FunctionsLoading } from "../../Main/Statistics/FunctionsLoading";
import { VariableTask } from "./VariableTask";
import SelectWithSearch from "../../General/GeneralComponent/SelectWithSearch";
import { CheckFont } from "../../General/GeneralComponent/CheckLang";
import { AbstructLang } from "../../General/GeneralComponent/AbstructLang";
import { useTranslation } from "react-i18next";

function AddTask({ medical_rep_id, date, id, t, langmiamed, open, setOpen }) {
  let dispatch = useDispatch();
  const { i18n } = useTranslation();
  const { TGbymedicalRep, loadingTG } = useSelectorCustom();
  const { LoadTargetByMedicalRepCustom } = FunctionsLoading();
  const { state, handleChangeTask, setLoad } = VariableTask();
  const submit = (e) => {
    e.preventDefault();
    dispatch(
      postTask(
        id,
        state.note,
        state.priority_level,
        state.time,
        date,
        state.status,
        state.target_group_id,
        setLoad
      )
    );
  };
  const { translate } = AbstructLang();
  return (
    <RModal
      open={open}
      onClose={() => {
        setOpen(false);
      }}
      center
      classNames={{
        modal: "addPop",
      }}
      focusTrapped={false}
    >
      <div className="div_addPop" dir={langmiamed == "ar" ? "rtl" : "ltr"}>
        <h3
          style={{ fontFamily: CheckFont(t("addTask")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("addTask")}
        </h3>
        <div
          style={{ fontFamily: CheckFont(t("time")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          <span>{t("time")}</span>
        </div>
        <input
          style={{
            fontFamily: i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
          }}
          type="time"
          onChange={(e) => handleChangeTask(e.target.value, "time")}
        />
        <div
          style={{ fontFamily: CheckFont(t("priority")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          <span>{t("priority")}: </span>
        </div>
        <select
          style={{
            fontFamily: state.priority_level
              ? CheckFont(translate(state.priority_level))
              : CheckFont(t("priority")),
          }}
          onChange={(e) => handleChangeTask(e.target.value, "priority_level")}
        >
          <option
            disabled="disabled"
            selected="selected"
            style={{ fontFamily: CheckFont(t("priority")) }}
          >
            {t("priority")}
          </option>
          <option style={{ fontFamily: CheckFont(t("high")) }} value="high">
            {t("high")}
          </option>
          <option style={{ fontFamily: CheckFont(t("Medium")) }} value="medium">
            {t("Medium")}
          </option>
          <option style={{ fontFamily: CheckFont(t("low")) }} value="low">
            {t("low")}
          </option>
        </select>
        <div
          style={{ fontFamily: CheckFont(t("target_group")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("target_group")}:{" "}
        </div>
        <SelectWithSearch
          load={loadingTG}
          array={TGbymedicalRep}
          setId={(value) => handleChangeTask(value, "target_group_id")}
          func={LoadTargetByMedicalRepCustom}
          loadID={medical_rep_id}
          typeCss={true}
        />
        <div
          style={{ fontFamily: CheckFont(t("note")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("note")}:{" "}
        </div>
        <textarea
          style={{
            fontFamily: i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
          }}
          onChange={(e) => handleChangeTask(e.target.value, "note")}
        />
        <div
          style={{ fontFamily: CheckFont(t("status")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("status")}:{" "}
        </div>
        <select
          style={{
            fontFamily: state.status
              ? CheckFont(translate(state.status))
              : CheckFont(t("status")),
          }}
          onChange={(e) => handleChangeTask(e.target.value, "status")}
        >
          <option
            disabled="disabled"
            selected="selected"
            style={{ fontFamily: CheckFont(t("status")) }}
          >
            {t("status")}
          </option>
          <option style={{ fontFamily: CheckFont(t("done")) }} value="is_done">
            {t("done")}
          </option>
          <option
            style={{ fontFamily: CheckFont(t("notdone")) }}
            value="not_done"
          >
            {t("notdone")}
          </option>
        </select>
        <button onClick={submit} style={{ fontFamily: CheckFont(t("save")) }}>
          {state.Load ? "Loading ..." : `${t("save")}`}
        </button>
      </div>
    </RModal>
  );
}

export default AddTask;
