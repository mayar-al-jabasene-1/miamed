import React, { useState, useEffect } from "react";
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import NoData from "../../../General/NoData/NoData";
import { CheckFont } from "../../../General/GeneralComponent/CheckLang";
import { useTranslation } from "react-i18next";

function DetailsProducts({ productArr, langmiamed, t, open, setOpen }) {
  const { i18n } = useTranslation();
  return (
    <RModal
      open={open}
      onClose={() => {
        setOpen(false);
        //dispatch(emptyTaskVisites())
      }}
      center
      classNames={{
        modal: "modal_popUp_DetailsProducts",
      }}
    >
      <div
        className="popUp_DetailsProducts"
        dir={langmiamed == "ar" ? "rtl" : "ltr"}
      >
        {productArr.map((pp) => {
          return (
            <div className="one_products_visite">
              {pp.product.media_urls.length != 0 ? (
                <img src={pp.product.media_urls[0].url} />
              ) : (
                <div className="relatied_img"></div>
              )}
              <h5
                style={{ fontFamily: CheckFont(pp.product.name) }}
                className={langmiamed == "ar" ? "textRight" : ""}
              >
                <span style={{ fontFamily: CheckFont(t("Product_Name")) }}>
                  {t("Product_Name")}
                </span>{" "}
                :
                <span style={{ fontFamily: CheckFont(pp.product.name) }}>
                  {pp.product.name}
                </span>
              </h5>
              <h5
                style={{ fontFamily: CheckFont(pp.quantity) }}
                className={`top_minac ${
                  langmiamed == "ar" ? "textRight" : ""
                } `}
              >
                <span style={{ fontFamily: CheckFont(t("Quantity")) }}>
                  {t("Quantity")}
                </span>{" "}
                :
                <span
                  style={{
                    fontFamily:
                      i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
                  }}
                >
                  {pp.quantity}
                </span>
              </h5>
              <h5
                style={{ fontFamily: CheckFont(pp.product.product_type) }}
                className={`top_minac ${
                  langmiamed == "ar" ? "textRight" : ""
                } `}
              >
                <span
                  style={{ fontFamily: CheckFont(pp.product.product_type) }}
                >
                  {pp.product.product_type === "medicine_samples"
                    ? "free samples"
                    : pp.product.product_type}
                </span>
              </h5>
            </div>
          );
        })}
        {productArr.length == 0 && <NoData />}
      </div>
    </RModal>
  );
}

export default DetailsProducts;
