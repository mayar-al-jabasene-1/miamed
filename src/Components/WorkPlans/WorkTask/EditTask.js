import React, { useState, useEffect } from "react";
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import { useDispatch } from "react-redux";
import SearchSelect from "../../General/SelectSearch";
import { putTask } from "../../../Redux/Actions/ActionTask";
import { useSelectorCustom } from "../../General/GeneralComponent/useSelectorCustom";
import { VariableTask } from "./VariableTask";
import { FunctionsLoading } from "../../Main/Statistics/FunctionsLoading";
import SelectWithSearch from "../../General/GeneralComponent/SelectWithSearch";
import { CheckFont } from "../../General/GeneralComponent/CheckLang";
import { useTranslation } from "react-i18next";

function EditTask({ medical_rep_id, t, langmiamed, arr, open, setOpen }) {
  let dispatch = useDispatch();
  const { TGbymedicalRep, loadingTG } = useSelectorCustom();
  const { state, setState, handleChangeTask, setLoad } = VariableTask();
  const { LoadTargetByMedicalRepCustom } = FunctionsLoading();
  const { i18n } = useTranslation();
  useEffect(() => {
    setState((prevState) => ({
      ...prevState,
      date: arr.date,
      time: arr.time.replace(/\s*([AaPp][Mm])\s*/, ""),
      priority_level: arr.priority_level,
      status: arr.status,
      target_group_id: arr.target_group_id,
      target_group_name: arr.target_group_name,
      note: arr.note,
    }));
  }, []);
  const submit = (e) => {
    e.preventDefault();
    dispatch(
      putTask(
        arr.id,
        arr.work_plan_id,
        state.note,
        state.priority_level,
        state.time,
        state.date,
        state.status,
        state.target_group_id,
        setLoad
      )
    );
  };
  return (
    <RModal
      open={open}
      onClose={() => {
        setOpen(false);
      }}
      center
      classNames={{
        modal: "addPop",
      }}
      focusTrapped={false}
    >
      <div className="div_addPop" dir={langmiamed == "ar" ? "rtl" : "ltr"}>
        <h3
          style={{ fontFamily: CheckFont(t("editTask")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("editTask")}
        </h3>
        <div
          style={{ fontFamily: CheckFont(t("time")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          <span>{t("time")}</span>
        </div>
        <input
          value={state.time}
          style={{
            fontFamily: i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
          }}
          type="time"
          onChange={(e) => handleChangeTask(e.target.value, "time")}
        />
        <div
          style={{ fontFamily: CheckFont(t("priority")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          <span>{t("priority")}: </span>
        </div>
        <select
          value={state.priority_level}
          style={{ fontFamily: CheckFont(t(state.priority_level)) }}
          onChange={(e) => handleChangeTask(e.target.value, "priority_level")}
        >
          <option
            disabled="disabled"
            selected="selected"
            style={{ fontFamily: CheckFont(t("priority")) }}
          >
            {t("priority")}
          </option>
          <option value="high">{t("high")}</option>
          <option value="medium">{t("Medium")}</option>
          <option value="low">{t("Medium")}</option>
        </select>
        <div
          style={{ fontFamily: CheckFont(t("target_group")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("target_group")}:{" "}
        </div>
        <div style={{ marginLeft: "10px" }}>
          <SelectWithSearch
            load={loadingTG}
            array={TGbymedicalRep}
            setId={(value) => handleChangeTask(value, "target_group_id")}
            func={LoadTargetByMedicalRepCustom}
            loadID={medical_rep_id}
            left={true}
            typeCss={true}
            previousID={state.target_group_id}
            name={state.target_group_name}
          />
        </div>
        <div
          style={{ fontFamily: CheckFont(t("note")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("note")}:{" "}
        </div>
        <textarea
          value={state.note}
          style={{
            fontFamily: state.note
              ? CheckFont(state.note)
              : i18n.language === "ar"
              ? "GE SS TWO"
              : "Calibri Bold",
          }}
          onChange={(e) => handleChangeTask(e.target.value, "note")}
        />
        <div
          style={{ fontFamily: CheckFont(t("status")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("status")}:{" "}
        </div>
        <select
          value={state.status}
          style={{ fontFamily: CheckFont(t(state.status)) }}
          onChange={(e) => handleChangeTask(e.target.value, "status")}
        >
          <option
            disabled="disabled"
            selected="selected"
            style={{ fontFamily: CheckFont(t("status")) }}
          >
            {t("status")}
          </option>
          <option style={{ fontFamily: CheckFont(t("done")) }} value="is_done">
            {t("done")}
          </option>
          <option
            style={{ fontFamily: CheckFont(t("notdone")) }}
            value="not_done"
          >
            {t("notdone")}
          </option>
        </select>
        <button onClick={submit} style={{ fontFamily: CheckFont(t("edit")) }}>
          {state.Load ? "Loading ..." : `${t("edit")}`}
        </button>
      </div>
    </RModal>
  );
}

export default EditTask;
