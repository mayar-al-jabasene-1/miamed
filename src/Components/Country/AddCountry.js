import React,{useState} from 'react'
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import { useDispatch } from "react-redux";
import { postCountry } from '../../Redux/Actions/ActionCountries';
import { CheckFont } from '../General/GeneralComponent/CheckLang';

function AddCountry({open ,setOpen , langmiamed ,t}) {
    let dispatch = useDispatch();
    const [name , setName] = useState('')
    const [Load , setLoad] = useState(false)
    const FunSend = (e) => {
        e.preventDefault()
        dispatch(postCountry(name,setLoad))
    }
    return (
        <RModal
            open={open}
            onClose={() => {
                setOpen(false);
            }}
            center
            classNames={{
                modal: "addPop",
            }}
            focusTrapped={false}
        >
            <div className='div_addPop' dir={langmiamed == 'ar'? "rtl" : "ltr"} >
                <h3 style={{fontFamily: CheckFont(t('addCountry'))}} className={langmiamed == 'ar'? "textRight": ""} >{t('addCountry')}</h3>
                <input style={{fontFamily: CheckFont(t('enterCountryName'))}} placeholder={t('enterCountryName')} type="text"  onChange={(e)=> setName(e.target.value)}  />
                <button onClick={FunSend} style={{fontFamily: CheckFont(t('save'))}} >
                    {
                        Load ? "Loading ..." : `${t('save')}`
                    }
                </button>
            </div>
        </RModal>
    )
}

export default AddCountry