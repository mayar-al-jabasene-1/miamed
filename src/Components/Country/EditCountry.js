import React, { useState } from "react";
import { Modal as RModal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import { useDispatch } from "react-redux";
import { putCountry } from "../../Redux/Actions/ActionCountries";
import { CheckFont } from "../General/GeneralComponent/CheckLang";

function EditCountry({ arr, open, setOpen, langmiamed, t }) {
  let dispatch = useDispatch();
  const [name, setName] = useState(arr.name);
  const [Load, setLoad] = useState(false);
  const FunSend = (e) => {
    e.preventDefault();
    dispatch(putCountry(arr.id, name, setLoad));
  };
  return (
    <RModal
      open={open}
      onClose={() => {
        setOpen(false);
      }}
      center
      classNames={{
        modal: "addPop",
      }}
      focusTrapped={false}
    >
      <div className="div_addPop" dir={langmiamed == "ar" ? "rtl" : "ltr"}>
        <h3
          style={{ fontFamily: CheckFont(t("editCountry")) }}
          className={langmiamed == "ar" ? "textRight" : ""}
        >
          {t("editCountry")}
        </h3>
        <input
          style={{ fontFamily: CheckFont(name) }}
          defaultValue={name}
          placeholder={t("enterCountryName")}
          type="text"
          onChange={(e) => setName(e.target.value)}
        />
        <button onClick={FunSend} style={{ fontFamily: CheckFont(t("edit")) }}>
          {Load ? "Loading ..." : `${t("edit")}`}
        </button>
      </div>
    </RModal>
  );
}

export default EditCountry;
