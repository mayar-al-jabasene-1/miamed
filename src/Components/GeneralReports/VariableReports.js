import { useState } from "react";
import { CheckFont } from "../General/GeneralComponent/CheckLang";
import { useTranslation } from "react-i18next";

export const VariableReports = () => {
  const [state, setState] = useState({
    openMap: false,
    lat: false,
    lng: false,
    isTap: "visits",
  });
  const storeMap = (latt, lngg) => {
    setState((prevState) => ({
      ...prevState,
      lat: latt,
      lng: lngg,
      openMap: true,
    }));
  };
  const setIsTap = (value) => {
    setState((prevState) => ({
      ...prevState,
      isTap: value,
    }));
  };
  const setIsOpen = (value) => {
    setState((prevState) => ({
      ...prevState,
      openMap: value,
    }));
  };
  return {
    ...state,
    state,
    setState,
    storeMap,
    setIsTap,
    setIsOpen,
  };
};

export const ColumnArrayReports = (translate, storeMap) => {
  const { t, i18n } = useTranslation();
  const Column = [
    {
      name: <span className="color_spann">{translate("target_group")}</span>,
      sortable: true,
      cell: (row) => (
        <span style={{ fontFamily: CheckFont(row.target_group_name) }}>
          {row["target_group_name"]}
        </span>
      ),
    },
    {
      name: <span className="color_spann">{translate("address")}</span>,
      sortable: true,
      cell: (row) => (
        <span style={{ fontFamily: CheckFont(row.target_group_address) }}>
          {row["target_group_address"]}
        </span>
      ),
    },
    {
      name: <span className="color_spann">{translate("delegates")}</span>,
      sortable: true,
      cell: (row) => (
        <span style={{ fontFamily: CheckFont(row.medical_rep_name) }}>
          {row["medical_rep_name"]}
        </span>
      ),
      width: "150px",
    },
    {
      name: <span className="color_spann">{translate("target_type")}</span>,
      sortable: true,
      cell: (row) => (
        <span style={{ fontFamily: CheckFont(row.target_group_type) }}>
          {row["target_group_type"]}
        </span>
      ),
    },
    {
      name: <span className="color_spann">{translate("location")}</span>,
      sortable: true,
      cell: (row) => (
        <span>
          {row.site_match ? (
            <i className="las la-check checkmatch"></i>
          ) : (
            <i className="las la-times falsematch"></i>
          )}
        </span>
      ),
    },
    {
      name: <span className="color_spann">{translate("reportType")}</span>,
      sortable: true,
      cell: (row) => (
        <span style={{ fontFamily: CheckFont(row.report_type_name) }}>
          {row["report_type_name"]}
        </span>
      ),
    },
    {
      name: <span className="color_spann">{translate("date")}</span>,
      sortable: true,
      cell: (row) => (
        <span
          style={{
            fontFamily: i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
          }}
        >
          {row["visit_date"]}
        </span>
      ),
    },
    {
      name: <span className="color_spann">{translate("hour")}</span>,
      sortable: true,
      cell: (row) => (
        <span
          style={{
            fontFamily: i18n.language === "ar" ? "GE SS TWO" : "Calibri Bold",
          }}
        >
          {row["visit_time"]}
        </span>
      ),
    },
    {
      name: (
        <span width="100%" className="color_spann">
          {translate("description")}
        </span>
      ),
      sortable: true,
      cell: (row) => (
        <span style={{ fontFamily: CheckFont(row.visit_details) }}>
          {row["visit_details"]}
        </span>
      ),
      width: "250px",
    },
    {
      name: <span className="color_spann">{translate("map")}</span>,
      cell: (row) => (
        <i
          className="las la-map-marked icon_edit_table"
          onClick={() => storeMap(row.lat, row.lng)}
        ></i>
      ),
    },
  ];
  return {
    Column,
  };
};
