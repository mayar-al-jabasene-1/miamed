import React from 'react'
import Table2 from '../Table2/Table2'
import select from '../../images/icons/pointer.png'
import External_In_Map from '../External_Visites/External_In_Map';
import FilterStatistics from '../Main/Statistics/FilterStatistics';
import { useSelectorCustom } from '../General/GeneralComponent/useSelectorCustom';
import { ColumnArrayReports, VariableReports } from './VariableReports';

function GeneralTargetGroup({ langmiamed, t }) {
    const {reportTargetGroup,loagingreportTargetGroup} = useSelectorCustom()
    const reversedreportTargetGroup = [...reportTargetGroup].reverse()
    const {state  ,storeMap ,setIsOpen} = VariableReports()
    const {Column} = ColumnArrayReports(t , storeMap)
    return (
        <>
            <FilterStatistics t={t} langmiamed={langmiamed} params='target_type_general' excel='target_group_excel'  />
            <div className='users' dir={langmiamed == "ar" ? "rtl" : "ltr"} >
                {
                    loagingreportTargetGroup === false ? (
                        <Table2 col={Column} roo={reversedreportTargetGroup} />
                    ) : <div className='alternative' >
                        <img src={select} />
                        <br></br>
                        <p>{t('filterr')}</p>
                    </div>
                }
            </div>
            {
                state.openMap && (
                    <External_In_Map open={state.openMap} setOpen={setIsOpen} lat={state.lat} lng={state.lng} />
                )
            }
        </>
    )
}

export default GeneralTargetGroup