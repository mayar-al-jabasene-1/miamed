import React from 'react'
import Table2 from '../Table2/Table2'
import select from '../../images/icons/pointer.png'
import External_In_Map from '../External_Visites/External_In_Map';
import FilterStatistics from '../Main/Statistics/FilterStatistics';
import { useSelectorCustom } from '../General/GeneralComponent/useSelectorCustom';
import { ColumnArrayReports, VariableReports } from './VariableReports';

function GeneralReportsEX({ langmiamed, t,navigate,setopenInternet}) {
    const {generalExReport,loadinggeneralExReport} = useSelectorCustom()
    const reversedgeneralExReport = [...generalExReport].reverse()
    const {state  ,storeMap ,setIsOpen} = VariableReports()
    const {Column} = ColumnArrayReports(t , storeMap)
    return (
        <>
            <FilterStatistics t={t} langmiamed={langmiamed} params='external_visite_general' excel='external_visit_excel' />
            <div className='users' dir={langmiamed == "ar" ? "rtl" : "ltr"} >
                {
                    loadinggeneralExReport == false ? (
                        <Table2 col={Column} roo={reversedgeneralExReport} />
                    ) : <div className='alternative' >
                        <img src={select} />
                        <br></br>
                        <p>{t('filterr')}</p>
                    </div>
                }
            </div>
            {
                state.openMap && (
                    <External_In_Map open={state.openMap} setOpen={setIsOpen} lat={state.lat} lng={state.lng} />
                )
            }
        </>
    )
}

export default GeneralReportsEX