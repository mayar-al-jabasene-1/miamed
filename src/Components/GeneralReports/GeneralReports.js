import React, { useEffect, useState } from 'react'
import Page_Header from '../General/Page_Header';
import './GeneralReports.css'
import Table2 from '../Table2/Table2';
import NoInternet from '../General/NotPermission/NoInternet';
import External_In_Map from '../External_Visites/External_In_Map';
import GeneralReportsEX from './GeneralReportsEX';
import select from '../../images/icons/pointer.png'
import GeneralTargetGroup from './GeneralTargetGroup';
import FilterStatistics from '../Main/Statistics/FilterStatistics';
import { useSelectorCustom } from '../General/GeneralComponent/useSelectorCustom';
import { AbstructVar } from '../General/GeneralComponent/AbstructVar';
import { AbstructLang } from '../General/GeneralComponent/AbstructLang';
import { ColumnArrayReports, VariableReports } from './VariableReports';

function GeneralReports() {
    const [openInternet, setopenInternet] = useState(false)
    const {navigate , langmiamed} = AbstructVar()
    const {translate} = AbstructLang();
    const {generalReport,loadinggeneralReport} = useSelectorCustom()
    const reversedgeneralReport = [...generalReport].reverse()
    const {state  ,storeMap ,setIsTap,setIsOpen} = VariableReports()
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    const {Column} = ColumnArrayReports(translate , storeMap)
    return (
        <>
            <div className={langmiamed == "ar" ? "main-content_ar" : "main-content_en"}>
                <main>
                    <Page_Header langmiamed={langmiamed} header={translate('reports')} path={`${translate('home')} / ${translate('reports')} `} />
                </main>
                <div className="page-content" >
                    <div className='choise_task'  dir={langmiamed == "ar" ? "rtl" : "ltr"} >
                        <button className={state.isTap == 'visits' ? "active_btn_choise" : "unactive_btn_choise"} onClick={() => { setIsTap('visits') }} >{translate('work_plan_report')}</button>
                        <button className={state.isTap == 'visitsEX' ? "active_btn_choise" : "unactive_btn_choise"} onClick={() => { setIsTap('visitsEX') }}  >{translate('Scientific_Office')}</button>
                        <button className={state.isTap == 'visitsTarget' ? "active_btn_choise" : "unactive_btn_choise"} onClick={() => { setIsTap('visitsTarget') }}  >{translate('target_group_report')}</button>
                    </div>
                    {
                        state.isTap === 'visits' && (
                            <>
                                <FilterStatistics t={translate} langmiamed={langmiamed} params='work_plan_general' excel='work_plan_excel' />
                                <div className='users' dir={langmiamed == "ar" ? "rtl" : "ltr"} >
                                    {
                                        loadinggeneralReport == false ? (
                                            <Table2 col={Column} roo={reversedgeneralReport} />
                                        ) : <div className='alternative' >
                                            <img src={select} />
                                            <br></br>
                                            <p>{translate('filterr')}</p>
                                        </div>
                                    }
                                </div>
                            </>
                        )
                    }
                    {
                        state.isTap === 'visitsEX' && (
                            <GeneralReportsEX langmiamed={langmiamed} t={translate} setopenInternet={setopenInternet} navigate={navigate} />
                        )
                    }
                    {
                        state.isTap === 'visitsTarget' && (
                            <GeneralTargetGroup langmiamed={langmiamed} t={translate} setopenInternet={setopenInternet} navigate={navigate} />
                        )
                    }
                </div>
            </div>
            {
                openInternet && (
                    <NoInternet open={openInternet} setOpen={setopenInternet} />
                )
            }
            {
                state.openMap && (
                    <External_In_Map open={state.openMap} setOpen={setIsOpen} lat={state.lat} lng={state.lng} />
                )
            }
        </>
    )
}

export default GeneralReports